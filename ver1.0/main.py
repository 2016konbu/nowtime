import PySimpleGUI as sg
import datetime

# 画面構成
layout = [
    [sg.Text("テスト")],
    [sg.Button("test"), sg.Text("横に並べたい時はこう書く")],
    [sg.Button("test2", key="asd")]
]

# ウィンドウの生成
window = sg.Window("フォームタイトル", layout)

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "test":
        print("testボタンが押されました。")
    elif event == "asd":
        print("keyがasdのボタンが押されました。")

window.close()
