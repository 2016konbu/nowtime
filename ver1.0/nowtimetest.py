import datetime
import time


def GetNowTime():
    now = datetime.datetime.now()
    today = now.strftime('%Y/%m/%d %H:%M.%S.%MS')
    return today


cnt = 0

while True:
    today = GetNowTime()
    flg = today
    print(today)
    while today == flg:
        today = GetNowTime()
    cnt = cnt + 1
    if cnt > 10 ** 4:
        break

print(cnt)
